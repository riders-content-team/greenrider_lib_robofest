#!/usr/bin/env python2
import rospy
from geometry_msgs.msg import Point, PoseStamped
from greenrider_description.srv import CameraService
from gazebo_msgs.srv import GetModelState
from std_msgs.msg import Bool, Float64
import requests
import os
import time
import numpy as np
import cv2 as cv
from sensor_msgs.msg import Image, CompressedImage
from cv_bridge import CvBridge, CvBridgeError
from tf.transformations import quaternion_from_euler
from iiwa_msgs.msg import JointPosition
from iiwa_msgs.msg import JointQuantity
from sensor_msgs.msg import JointState

class RobotControl:

    def __init__(self, robot_name):
        self.robot_name = robot_name

        self.cv_bridge_1 = CvBridge()
        self.cv_pub_1 = rospy.Publisher("/cv_image_1", Image, queue_size=1000)

        self.cv_bridge_2 = CvBridge()
        self.cv_pub_2 = rospy.Publisher("/cv_image_2", Image, queue_size=1000)


        rospy.init_node('control', anonymous=True)
        self.pose_pub = rospy.Publisher('/' + self.robot_name + '/set_pose', Point, queue_size=1)
        self.pose_msg = Point()

        self.belt1_pub = rospy.Publisher('/contact/state', Bool, queue_size=1)
        self.belt1_msg = Bool()
        self.belt2_pub = rospy.Publisher('/contact2/state', Bool, queue_size=1)
        self.belt2_msg = Bool()

        self.grip_pub = rospy.Publisher('/' + self.robot_name + '/grip_state', Bool, queue_size=1)
        self.grip_msg = Bool()

        self.new_object_pub = rospy.Publisher('/' + self.robot_name + '/spawn_new_object', Bool, queue_size=1)
        self.new_obj_msg = Bool()

        self.image = self._Image()
        self.image_2 = self._Image()
        self.init_sensor_services()

        #arm 2
        self.pub2 = rospy.Publisher('/iiwa2/command/CartesianPose', PoseStamped, queue_size=10)
        self.pos2 = PoseStamped() 

        self.grabber_left_pub2 = rospy.Publisher('/iiwa2/grabber_left_joint/set_speed', Float64, queue_size=10)
        self.left_gripper_msg2 = Float64()

        self.grabber_right_pub2 = rospy.Publisher('/iiwa2/grabber_right_joint/set_speed', Float64, queue_size=10)
        self.right_gripper_msg2 = Float64()

        self.gripper_state2 = Bool()

        self.joint_pub2 = rospy.Publisher('iiwa2/command/JointPosition', JointPosition, queue_size=10)

        self.joint_values2 = JointPosition()
        self.joint_quantity2 = JointQuantity()

        self.joint_sub2 = rospy.Subscriber("/iiwa2/joint_states",JointState,self.callback_joints_2)
        self.pose_sub2 = rospy.Subscriber("/iiwa2/state/CartesianPose",PoseStamped,self.callback_arm_pose_2)
        #arm 1
        self.pub = rospy.Publisher('/iiwa/command/CartesianPose', PoseStamped, queue_size=10)
        self.pos = PoseStamped() 

        self.grabber_left_pub = rospy.Publisher('/iiwa/grabber_left_joint/set_speed', Float64, queue_size=10)
        self.left_gripper_msg = Float64()

        self.grabber_right_pub = rospy.Publisher('/iiwa/grabber_right_joint/set_speed', Float64, queue_size=10)
        self.right_gripper_msg = Float64()

        self.gripper_state = Bool()

        self.joint_pub = rospy.Publisher('iiwa/command/JointPosition', JointPosition, queue_size=10)

        self.joint_values = JointPosition()
        self.joint_quantity = JointQuantity()

        self.joint_sub = rospy.Subscriber("/iiwa/joint_states",JointState,self.callback_joints)
        self.pose_sub = rospy.Subscriber("/iiwa/state/CartesianPose",PoseStamped,self.callback_arm_pose)


    def init_sensor_services(self):
        try:
            rospy.wait_for_service("/greenrider/get_image", 1.0)
            self.image_data_service = rospy.ServiceProxy('/greenrider/get_image', CameraService)
            resp = self.image_data_service()

            self.camera_height = resp.height
            self.camera_width = resp.width

            self.image.height = resp.height
            self.image.width = resp.width

            rospy.wait_for_service("/greenrider_2/get_image_2", 1.0)
            self.image_data_service_2 = rospy.ServiceProxy('/greenrider_2/get_image_2', CameraService)
            resp_2 = self.image_data_service_2()

            self.camera_height_2 = resp_2.height
            self.camera_width_2 = resp_2.width

            self.image_2.height = resp_2.height
            self.image_2.width = resp_2.width

            return True
        except (rospy.ServiceException, rospy.ROSException):
            print("no camera sensor")
            return False 
    #arm 1
    def set_arm_pose_euler_1(self,posX,posY,posZ,roll,pitch,yaw):        
        self.pos.header.seq = 1
        self.pos.header.stamp = rospy.Time.now()
        self.pos.header.frame_id = "iiwa_link_0"

        self.pos.pose.position.x = posX
        self.pos.pose.position.y = posY
        self.pos.pose.position.z = posZ

        q = quaternion_from_euler(roll,pitch,yaw)

        self.pos.pose.orientation.x = q[0]
        self.pos.pose.orientation.y = q[1]
        self.pos.pose.orientation.z = q[2]
        self.pos.pose.orientation.w = q[3]      
        self.pub.publish(self.pos) 

    def set_arm_pose_1(self,pose):        
        self.pos.header.seq = 1
        self.pos.header.stamp = rospy.Time.now()
        self.pos.header.frame_id = "iiwa1_link_0"

        self.pos.pose.position.x = pose.position.x
        self.pos.pose.position.y = pose.position.y
        self.pos.pose.position.z = pose.position.z

        self.pos.pose.orientation.x = pose.orientation.x
        self.pos.pose.orientation.y = pose.orientation.y
        self.pos.pose.orientation.z = pose.orientation.z
        self.pos.pose.orientation.w = pose.orientation.w
        self.pub.publish(self.pos)

    
    def callback_arm_pose(self,msg):
        self.position = msg
        return self.position.pose

    def get_arm_pose(self):
        return self.position.pose
    
    def joint_control_1(self,joint1=0.0,joint2=0.0,joint3=0.0,joint4=0.0,joint5=0.0,joint6=0.0,joint7=0.0):
        self.joint_quantity.a1 = joint1
        self.joint_quantity.a2 = joint2
        self.joint_quantity.a3 = joint3
        self.joint_quantity.a4 = joint4
        self.joint_quantity.a5 = joint5
        self.joint_quantity.a6 = joint6
        self.joint_quantity.a7 = joint7

        self.joint_values.position = self.joint_quantity

        self.joint_pub.publish(self.joint_values)
    
    def callback_joints(self,msg):
        self.joint_states = msg
        return self.joint_states.position

    def get_joints_1(self):
        return self.joint_states.position

    def subsFonk(self,data):
        print(data)     
            
    def gripper_1(self, state):
        if state == True:
            self.left_gripper_msg.data = 0.4
            self.right_gripper_msg.data = -0.4
        else:
            self.left_gripper_msg.data = -0.2
            self.right_gripper_msg.data = 0.2

        self.grabber_left_pub.publish(self.left_gripper_msg)
        self.grabber_right_pub.publish(self.right_gripper_msg)
        #self.grip_msg.data = state
        #self.grip_pub.publish(self.grip_msg)
    
    #arm 2
    def set_arm_pose_euler_2(self,posX,posY,posZ,roll,pitch,yaw):        
        self.pos2.header.seq = 1
        self.pos2.header.stamp = rospy.Time.now()
        self.pos2.header.frame_id = "iiwa2_link_0"

        self.pos2.pose.position.x = posX
        self.pos2.pose.position.y = posY
        self.pos2.pose.position.z = posZ

        q = quaternion_from_euler(roll,pitch,yaw)

        self.pos2.pose.orientation.x = q[0]
        self.pos2.pose.orientation.y = q[1]
        self.pos2.pose.orientation.z = q[2]
        self.pos2.pose.orientation.w = q[3]      
        self.pub2.publish(self.pos2) 

    def set_arm_pose_2(self,pose):        
        self.pos2.header.seq = 1
        self.pos2.header.stamp = rospy.Time.now()
        self.pos2.header.frame_id = "iiwa2_link_0"

        self.pos2.pose.position.x = pose.position.x
        self.pos2.pose.position.y = pose.position.y
        self.pos2.pose.position.z = pose.position.z

        self.pos2.pose.orientation.x = pose.orientation.x
        self.pos2.pose.orientation.y = pose.orientation.y
        self.pos2.pose.orientation.z = pose.orientation.z
        self.pos2.pose.orientation.w = pose.orientation.w
        self.pub2.publish(self.pos2)

    
    def callback_arm_pose_2(self,msg):
        self.position2 = msg
        return self.position2.pose

    def get_arm_pose_2(self):
        return self.position2.pose
    
    def joint_control_2(self,joint1=0.0,joint2=0.0,joint3=0.0,joint4=0.0,joint5=0.0,joint6=0.0,joint7=0.0):
        self.joint_quantity2.a1 = joint1
        self.joint_quantity2.a2 = joint2
        self.joint_quantity2.a3 = joint3
        self.joint_quantity2.a4 = joint4
        self.joint_quantity2.a5 = joint5
        self.joint_quantity2.a6 = joint6
        self.joint_quantity2.a7 = joint7

        self.joint_values2.position = self.joint_quantity2

        self.joint_pub2.publish(self.joint_values2)
    
    def callback_joints_2(self,msg):
        self.joint_states2 = msg
        return self.joint_states2.position

    def get_joints_2(self):
        return self.joint_states2.position

    def subsFonk2(self,data):
        print(data)     
            
    def gripper_2(self, state):
        if state == True:
            self.left_gripper_msg2.data = 0.4
            self.right_gripper_msg2.data = -0.4
        else:
            self.left_gripper_msg2.data = -0.2
            self.right_gripper_msg2.data = 0.2

        self.grabber_left_pub2.publish(self.left_gripper_msg2)
        self.grabber_right_pub2.publish(self.right_gripper_msg2)
        self.grip_msg.data = state
        self.grip_pub.publish(self.grip_msg)
    
    def get_box_type(self):
        plastic_counter = 0
        metal_counter = 0
        paper_counter = 0
        image = self.image_data()
        for i in range(50,100):
            for j in range(20,100):
                a = (i + j * image.width) * 3 # index to start RGB components
                r = image.data[a+0]   # red component
                g = image.data[a+1]   # green component
                b = image.data[a+2]   # blue component
                if r > 25 and r < 50:
                    plastic_counter += 1
                elif r > 50 and r < 100:
                    paper_counter += 1
                elif r > 100:
                    metal_counter += 1
        print(plastic_counter,metal_counter,paper_counter)
        liste = [plastic_counter,metal_counter,paper_counter]

        if liste.index(max(liste)) == 0:
            print("This is plastic")
            return 0
        elif liste.index(max(liste)) == 1:
            print("This is metal")
            return 1
        elif liste.index(max(liste)) == 2:
            print("This is carton")
            return 2

    def wait_for_box(self):
        pixel_counter = 0
        while pixel_counter == 0:
            image = self.image_data()
            for i in range(50,100):
                y_offset = 15
                a = (i + y_offset*image.width) * 3
                r = image.data[a+0]   # red component
                g = image.data[a+1]   # green component
                b = image.data[a+2]   # blue component
                if r > 20:
                    pixel_counter += 1
            if pixel_counter > 0:
                print("Box detected, Stop Conveyor")

    #robot
    def new_object(self, state):
        self.new_obj_msg.data = state
        self.new_object_pub.publish(self.new_obj_msg)

    def start_conveyor_1(self, state):
        self.belt1_msg.data = state
        self.belt1_pub.publish(self.belt1_msg)

    def start_conveyor_2(self, state):
        self.belt2_msg.data = state
        self.belt2_pub.publish(self.belt2_msg)
    
    def grip(self, state):
        self.grip_msg.data = state
        self.grip_pub.publish(self.grip_msg)

    def set_position(self,x,y,z):
        self.pose_msg.x = x
        self.pose_msg.y = y
        self.pose_msg.z = z
        self.pose_pub.publish(self.pose_msg)
    
    def image_data(self):
        resp = self.image_data_service()
        self.image.data = []
        for pixel in resp.data:
            self.image.data.append(ord(pixel))
        #print(self.image.data)
        self.achieve(154)
        return self.image
    
    def image_data_2(self):
        resp = self.image_data_service_2()
        self.image_2.data = []
        for pixel in resp.data:
            self.image_2.data.append(ord(pixel))
        #print(self.image.data)
        #self.achieve(154)
        return self.image_2
    
    def get_OpenCV_image_1(self):
        resp = self.image_data_service()
        height = resp.height
        width = resp.width
        #print("w: ",width,"he:",height)
        #image_rgb_data = []

        im_arr = np.ndarray((width, height, 3), dtype=np.uint8)
        #image_rgb_data.append(ord(resp.data[i]))
        rcount = 0
        hcount = 0
        for i in range(self.camera_height * self.camera_width * 3):
            if(i % 3 == 0): #B
                #print("r",rcount,"h",hcount)
                im_arr[rcount][hcount][2] = ord(resp.data[i])
            elif(i % 3 == 1): #G
                im_arr[rcount][hcount][1] = ord(resp.data[i])
            else: #R
                im_arr[rcount][hcount][0] = ord(resp.data[i])
                hcount += 1
            if(hcount == width):
                rcount +=1
                hcount = 0
        return im_arr
    
    def plot_image_1(self, frame):
        img_message = self.cv_bridge_1.cv2_to_imgmsg(frame,encoding="bgr8")
        self.cv_pub_1.publish(img_message)
    

    def get_OpenCV_image_2(self):
        resp = self.image_data_service_2()
        height = resp.height
        width = resp.width
        #print("w: ",width,"he:",height)
        #image_rgb_data = []

        im_arr = np.ndarray((width, height, 3), dtype=np.uint8)
        #image_rgb_data.append(ord(resp.data[i]))
        rcount = 0
        hcount = 0
        for i in range(self.camera_height_2 * self.camera_width_2 * 3):
            if(i % 3 == 0): #B
                #print("r",rcount,"h",hcount)
                im_arr[rcount][hcount][2] = ord(resp.data[i])
            elif(i % 3 == 1): #G
                im_arr[rcount][hcount][1] = ord(resp.data[i])
            else: #R
                im_arr[rcount][hcount][0] = ord(resp.data[i])
                hcount += 1
            if(hcount == width):
                rcount +=1
                hcount = 0
        return im_arr
    
    def plot_image_2(self, frame):
        img_message = self.cv_bridge_2.cv2_to_imgmsg(frame,encoding="bgr8")
        self.cv_pub_2.publish(img_message)



    def is_ok(self):
        if not rospy.is_shutdown():
            return True
        else:
            return False

    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


    class _Image:
        def __init__(self):
            self.height = 0
            self.width = 0
            self.data = []
